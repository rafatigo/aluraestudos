<?php

use Alura\Banco\Modelo\Conta\{ContaPoupanca, Conta, ContaCorrente, Titular};
use Alura\Banco\Modelo\{Cpf, Endereco};

require_once "autoload.php";

$conta = new \Alura\Banco\Modelo\Conta\ContaCorrente(new Titular(new Cpf("134.743.740-10"), "Tiago Rafael", new Endereco("Maceío", "Serraria", "Paulo Lobo Assumpção", "153")));

$conta->depositar(500);
$conta->sacar(100);

echo $conta->getSaldo();