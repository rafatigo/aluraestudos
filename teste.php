<?php

require_once "autoload.php";

$tiago = new Titular(new Cpf("571.275.650-15"), "Tiago Rafael");
$novaConta = new Conta($tiago);
$novaConta->depositar(550);

$diogo = new Titular(new Cpf("134.743.740-10"), "Diogo José");
$segundaConta = new Conta($diogo);
$segundaConta->sacar(320);

$airton = new Titular(new Cpf("636.451.540-19"), "Airton José dos Santos");
$terceiraConta = new Conta($airton);

$camile = new Titular(new Cpf("357.734.730-97"), "Camile Lanne");
$quartaConta = new Conta($camile);
$quartaConta->transferir(1200, $terceiraConta);

$renilda = new Titular(new Cpf("900.274.170-77"), "Maria Renilda");
$novaConta = new Conta($renilda);
$novaConta->depositar(5000);

new titular(new Cpf("722.552.710-01"), "Tiago Rafael");

$numeroDeContas = Conta::getNumeroDeContas();
echo "<p>{$numeroDeContas}</p>";

var_dump($novaConta, $segundaConta, $terceiraConta, $quartaConta);

