<?php

require_once "autoload.php";

use Alura\Banco\Service\Autenticador;
use Alura\Banco\Modelo\Funcionario\Diretor;
use \Alura\Banco\Modelo\Cpf;

$autenticador = new Autenticador();
$diretor = new \Alura\Banco\Modelo\Conta\Titular(new Cpf("941.984.120-06"),"Tiago Rafael", new \Alura\Banco\Modelo\Endereco("Maceió", "Serraria", "Dos Bobos", "666"));

$autenticador->tentaLogin($diretor, "abcd");