<?php

require_once 'autoload.php';

//require_once "src/Modelo/Conta/Conta.php";
//require_once "src/Modelo/Conta/Titular.php";
//require_once "src/Modelo/Cpf.php";
//require_once "src/Modelo/Endereco.php";
//require_once "src/Modelo/Pessoa.php";

//$tiago = new Titular(new Cpf("134.743.740-10"), "Amanda Thaysa", new Endereco("Maceío", "Serraria", "Paulo Lobo Assumpção", "153"));
$amanda = new \Alura\Banco\Modelo\Conta\Titular(new \Alura\Banco\Modelo\Cpf('134.743.740-10'), "Amanda Thaysa", new \Alura\Banco\Modelo\Endereco("Maceío", "Serraria", "Paulo Lobo Assumpção", "153"));
$quintaConta = new \Alura\Banco\Modelo\Conta\Conta($amanda);
$quintaConta->depositar(500);
$quintaConta->sacar(300); //Isso pode.
//$quintaConta->setSaldo(300); //Isso não pode.

echo "<h3>O saldo da conta de {$amanda->getNome()} é de {$quintaConta->getSaldo()}</h3>";

var_dump($amanda, $quintaConta);