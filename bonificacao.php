<?php

use Alura\Banco\Modelo\Cpf;
use Alura\Banco\Modelo\Funcionario\{Gerente, Desenvolvedor, EditorVideo};
use Alura\Banco\Service\ControladorDeBonificacao;

require_once "autoload.php";

$desenvolvedor = new Desenvolvedor("Tiago Rafael", new Cpf("077.211.594-07"), "3000");
$gerente = new Gerente("Airton José", new Cpf("714.708.240-00"), "3000");
$editorVideo = new EditorVideo("Diogo José", new Cpf("748.922.610-41"), "2500");

$desenvolvedor->sobeDeNivel();

$controlador = new ControladorDeBonificacao();
$controlador->adicionarbonificacaoDe($desenvolvedor);
$controlador->adicionarbonificacaoDe($gerente);
$controlador->adicionarbonificacaoDe($editorVideo);
echo $controlador->recuperaTotal();

var_dump($desenvolvedor, $gerente, $editorVideo);
