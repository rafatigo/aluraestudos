<?php


namespace Alura\Banco\Modelo\Conta;


class ContaCorrente extends Conta
{
    protected function percentualTarifa(): float
    {
        return 0.05;
    }

    /**
     * @param float $valor
     * @param Conta $contaDeDestino
     */
    public function transferir(float $valor, Conta $contaDeDestino): void
    {
        if ($valor > $this->saldo) {
            echo "Saldo Indisponível";
            return;
        }
        $this->sacar($valor);
        $contaDeDestino->depositar($valor);
    }

}