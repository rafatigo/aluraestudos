<?php

namespace Alura\Banco\Modelo\Conta;

/**
 * Class Conta
 */
abstract class Conta
{
    // Definir os dados das contas

    /** @var Titular */
    private Titular $titular;

    /** @var float */
    protected float $saldo;

    /** @var int */
    private static int $numeroDeContas = 0;

    //Métodos

    /**
     * Conta constructor.
     * @param string $cpfTitular
     * @param string $nomeTitular
     */
    public function __construct(Titular $titular)
    {
        $this->titular = $titular;
        $this->saldo = 0;

//        Conta::$numeroDeContas++;
        self::$numeroDeContas++;
    }

    /**
     *
     */
    public function __destruct()
    {
        if (self::$numeroDeContas > 2)
        {
            echo "Há mais de uma conta ativa <br />";
        }
    }

    /**
     * @return float
     */
    public function getSaldo()
    {
        return $this->saldo;
    }

    /**
     * @param float $valor
     */
    public function sacar(float $valorASacar): void
    {
        $tarifaSaque = $valorASacar * $this->percentualTarifa();

        $valorSaque = $valorASacar + $tarifaSaque;
        if ($valorSaque > $this->saldo) {
            echo "Saldo indisponível";
            return;
        }
        $this->saldo -= $valorSaque;
    }

    /**
     * @param float $valor
     */
    public function depositar(float $valor): void
    {
        if ($valor < 0) {
            echo "O valor tem que ser positivo";
            return;
        }
        $this->saldo += $valor;
    }

    /**
     * @return int
     */
    public static function getNumeroDeContas(): int
    {
        return self::$numeroDeContas;
    }

    abstract protected function percentualTarifa(): float;

}
