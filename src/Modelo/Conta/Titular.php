<?php

namespace Alura\Banco\Modelo\Conta;

use Alura\Banco\Modelo\Autenticavel;
use Alura\Banco\Modelo\Cpf;
use Alura\Banco\Modelo\Endereco;
use Alura\Banco\Modelo\Pessoa;

include "src/Modelo/Pessoa.php";


/**
 * Class Titular
 */
class Titular extends Pessoa implements Autenticavel
{
    /**
     * @var Endereco
     */
    private Endereco $endereco;

    /**
     * Titular constructor.
     * @param string $cpf
     * @param string $nome
     */
    public function __construct(Cpf $cpf, string $nome, Endereco $endereco)
    {
        parent::__construct($nome, $cpf);
        $this->endereco = $endereco;
    }

    /**
     * @return Endereco
     */
    public function getEndereco(): Endereco
    {
        return $this->endereco;
    }

    /**
     * @param string $senha
     * @return bool
     */
    public function podeAutenticar(string $senha): bool
    {
        return $senha == 'abcd';
    }

}