<?php

namespace Alura\Banco\Modelo;


/**
 * Class Pessoa
 */
abstract class Pessoa
{
    /**
     * @var string
     */
    protected string $nome;
    /**
     * @var string
     */
    protected Cpf $cpf;

    /**
     * Pessoa constructor.
     * @param $nome
     * @param $cpf
     */
    public function __construct($nome, Cpf $cpf)
    {
        $this->validaTamanhoNomeTitular($nome);
        $this->nome = $nome;
        $this->cpf = $cpf;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @return mixed
     */
    public function getCpf()
    {
        return $this->cpf;
    }


    /**
     * @param string $nomeTitular
     */
    protected function validaTamanhoNomeTitular(string $nomeTitular)
    {
        if (strlen($nomeTitular) < 5)
        {
            echo "O nome precisa ter no mínimo 5 caracteres.";
            exit();
        }
    }

}