<?php

namespace Alura\Banco\Modelo\Funcionario;


use Alura\Banco\Modelo\Cpf;
use Alura\Banco\Modelo\Pessoa;

/**
 * Class Funcionario
 */
abstract class Funcionario extends Pessoa
{
    /** @var float */
    private float $salario;

    /**
     * Funcionario constructor.
     * @param string $nome
     * @param string $cpf
     * @param string $salario
     */
    public function __construct(string $nome, Cpf $cpf, float $salario)
    {
        parent::__construct($nome, $cpf);
        $this->salario = $salario;
    }

    /**
     * @return float
     */
    public function getSalario()
    {
        return $this->salario;
    }

    /**
     * @param string $nome
     */
    public function setNome(string $nome): void
    {
        $this->validaTamanhoNomeTitular($nome);
        $this->nome = $nome;
    }

    /**
     * @param float $valorAumento
     */
    public function recebeAumento(float $valorAumento): void
    {
        if ($valorAumento < 0) {
            echo "Aumento tem que ser positivo";
            return;
        }

        $this->salario += $valorAumento;
    }

    abstract public function calculaBonificacao(): float;
}