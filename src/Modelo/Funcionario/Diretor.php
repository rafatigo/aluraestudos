<?php


namespace Alura\Banco\Modelo\Funcionario;


use Alura\Banco\Modelo\Autenticavel;

/**
 * Class Diretor
 * @package Alura\Banco\Modelo\Funcionario
 */
class Diretor extends Funcionario implements Autenticavel
{
    /**
     * @return float
     */
    public function calculaBonificacao(): float
    {
        return $this->getSalario() * 2;
    }

    /**
     * @param string $senha
     * @return bool
     */
    public function podeAutenticar(string $senha): bool
    {
        return $senha === '1234';
    }
}