<?php


namespace Alura\Banco\Modelo\Funcionario;


use Alura\Banco\Modelo\Autenticavel;

/**
 * Class Gerente
 * @package Alura\Banco\Modelo\Funcionario
 */
class Gerente extends Funcionario implements Autenticavel
{
    /**
     * @return float
     */
    public function calculaBonificacao(): float
    {
        return $this->getSalario();
    }

    /**
     * @param string $senha
     * @return bool
     */
    public function podeAutenticar(string $senha): bool
    {
        return $senha == '4321';
    }

}