<?php

namespace Alura\Banco\Modelo;


/**
 * Class Endereco
 * @package Alura\Banco\Modelo
 * @property-read $cidade
 * @property-read $bairro
 * @property-read $rua
 * @property-read $numero
 */
class Endereco
{
    /**
     * @var string
     */
    private string $cidade;
    /**
     * @var string
     */
    private string $bairro;
    /**
     * @var string
     */
    private string $rua;
    /**
     * @var string
     */
    private string $numero;

    /**
     * Endereco constructor.
     * @param string $cidade
     * @param string $bairro
     * @param string $rua
     * @param string $numero
     */
    public function __construct(string $cidade, string $bairro, string $rua, string $numero)
    {
        $this->cidade = $cidade;
        $this->bairro = $bairro;
        $this->rua = $rua;
        $this->numero = $numero;
    }

    /**
     * @return string
     */
    public function getCidade(): string
    {
        return $this->cidade;
    }

    /**
     * @return string
     */
    public function getBairro(): string
    {
        return $this->bairro;
    }

    /**
     * @return string
     */
    public function getRua(): string
    {
        return $this->rua;
    }

    /**
     * @return string
     */
    public function getNumero(): string
    {
        return $this->numero;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return "{$this->rua}, {$this->numero}, {$this->bairro}, {$this->cidade}";
    }


    /**
     * @param string $name
     * @return mixed
     */
    public function __get(string $nomeAtributo)
    {
        $metodo = 'get' . ucfirst($nomeAtributo);
        return $this->$metodo();
    }


}