<?php


namespace Alura\Banco\Service;


use Alura\Banco\Modelo\Funcionario\Funcionario;

/**
 * Class ControladorDeBonificacao
 * @package Alura\Banco\Service
 */
class ControladorDeBonificacao
{
    /**
     * @var int
     */
    private $totalBonificacoes = 0;

    /**
     * @param Funcionario $funcionario
     */
    public function adicionarbonificacaoDe(Funcionario $funcionario)
    {
        $this->totalBonificacoes += $funcionario->calculaBonificacao();
    }

    /**
     * @return float
     */
    public function recuperaTotal(): float
    {
        return $this->totalBonificacoes;
    }

}