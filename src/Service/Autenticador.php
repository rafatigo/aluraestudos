<?php


namespace Alura\Banco\Service;

use Alura\Banco\Modelo\Conta\Titular;
use Alura\Banco\Modelo\Funcionario\Diretor;
use Alura\Banco\Modelo\Funcionario\Gerente;

class Autenticador
{
    public function tentaLogin(Titular $diretor, string $senha): void
    {
        if ($diretor->podeAutenticar($senha)){
            echo "Ok. Usuário logado no sistema";
        }else{
            echo "Ops... Senha incorreta";
        }
    }

}